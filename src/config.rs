use std::borrow::Cow;

use camino::{Utf8Path, Utf8PathBuf};
use color_eyre::eyre::{anyhow, bail, Context, Result};
use log::warn;
use semver::Version;
use serde::{de::Error, Deserialize};

use std::collections::HashMap;

use crate::{
    libraries::{GitLibrary, Library, PathLibrary},
    nextpnr::{Ecp5Args, Ecp5Device, GowinArgs, Ice40Args, Ice40Device, NextpnrExtraArgs},
    pnr::{PnrResult, PnrTool},
    util::pluralize,
};

pub fn default_simulator() -> String {
    "icarus".into()
}

fn default_testbench_dir() -> Utf8PathBuf {
    "test".into()
}

#[derive(Deserialize, Debug, PartialEq, Default)]
pub struct ImportVerilog {
    /// Search paths for Verilog include directives.
    pub include: Option<Vec<Utf8PathBuf>>,

    /// Paths to Verilog files to import. Supports glob syntax.
    pub sources: Option<Vec<String>>,
}

#[derive(Deserialize, Debug, PartialEq)]
pub struct Simulation {
    /// Directory containing all test benches
    #[serde(default = "default_testbench_dir")]
    pub testbench_dir: Utf8PathBuf,
    /// `testbench_dir` but as originally specified in the toml file for easier-to-read testbench
    /// lists. Is skipped by serialization and initialized by the [Config::replace_relative_paths] function
    #[serde(skip)]
    pub testbench_dir_original: Utf8PathBuf,

    /// Extra dependencies to install to the test venv via pip
    pub python_deps: Option<Vec<String>>,

    /// The simulator to use as the cocotb backend. Currently verified to support verilator and
    /// icarus, but other simulators supported by cocotb may also work.
    ///
    /// Defaults to 'icarus'
    ///
    /// Requires a relatively recent version of verilator
    #[serde(default = "default_simulator")]
    pub simulator: String,

    /// The C++ version to use when compiling verilator test benches. Anything that
    /// clang or gcc accepts in the -std= field works, but the verilator wrapper requires
    /// at least c++17.
    /// Defaults to c++17
    pub cpp_version: Option<String>,

    /// Extra arguments to pass to verilator when building C++ test benches. Supports substituting
    /// `#ROOT_DIR#` to get project-relative directories
    pub verilator_args: Option<Vec<String>>,
}

impl Default for Simulation {
    fn default() -> Self {
        Self {
            testbench_dir: default_testbench_dir(),
            simulator: default_simulator(),

            testbench_dir_original: Default::default(),
            python_deps: Default::default(),
            cpp_version: Default::default(),
            verilator_args: Default::default(),
        }
    }
}

#[derive(Deserialize, Debug, PartialEq)]
pub struct Synthesis {
    /// The name of the unit to use as a top module for the design. The name must
    /// be an absolute path to the unit, for example `proj::main::top`, unless the
    /// module is marked `#[no_mangle]` in which case the name is used.
    ///
    /// Can also be set to the name of a module defined in verilog if a pure verilog top
    /// is desired.
    pub top: String,
    /// The yosys command to use for synthesis
    pub command: String,

    /// Extra verilog files only needed during the synthesis process.
    pub verilog: Option<ImportVerilog>,
}

impl Synthesis {
    pub fn extra_verilog_files(&self, root: &Utf8Path) -> Result<Vec<Utf8PathBuf>> {
        discarding_resolve_globs(
            root,
            self.verilog
                .as_ref()
                .and_then(|verilog| verilog.sources.as_ref())
                .unwrap_or(&vec![]),
        )
    }

    pub fn verilog_include_paths(&self, root: &Utf8Path) -> Vec<Utf8PathBuf> {
        self.verilog
            .as_ref()
            .and_then(|verilog| verilog.include.as_ref())
            .cloned()
            .unwrap_or_default()
            .into_iter()
            .map(|path| root.join(path))
            .collect()
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct NextPnrArgs<DeviceArgs> {
    #[serde(flatten)]
    pub device_args: DeviceArgs,
    /// If set, inputs and outputs of the top module do not need a corresponding field
    /// in the pin file. This is helpful for benchmarking when pin mapping is irreleveant, but
    /// when running in hardware, it is recommended to leave this off in order to get a warning
    /// when pins aren't set in the pin file.
    #[serde(default)]
    pub allow_unconstrained: bool,
    /// Continue to the upload step even if the timing isn't met.
    /// This is helpful when you suspect that the place-and-route tool is conservative
    /// with its timing requirements, but gives no guarantees about correctness.
    #[serde(default)]
    pub allow_timing_fail: bool,
    /// The path to a file which maps inputs and outputs of your top module to physical pins.
    /// On ECP5 chips, this is a `pcf` file, and on iCE40, it is an `lpf` file.
    pub pin_file: Utf8PathBuf,
}

impl<DeviceArgs: 'static + NextpnrExtraArgs> NextPnrArgs<DeviceArgs> {
    fn make_dyn(self, pin_format: &'static str) -> Result<NextPnrArgs<Box<dyn NextpnrExtraArgs>>> {
        self.pin_file
            .extension()
            .ok_or(anyhow!(
                "The pin file must have an extension. Got {}",
                self.pin_file
            ))
            .and_then(|ext| {
                if ext != pin_format {
                    bail!(
                        "The pin file for this architecture must be {}. Got {}",
                        pin_format,
                        self.pin_file
                    )
                } else {
                    Ok(())
                }
            })?;

        Ok(NextPnrArgs {
            device_args: Box::new(self.device_args),
            allow_timing_fail: self.allow_timing_fail,
            allow_unconstrained: self.allow_unconstrained,
            pin_file: self.pin_file,
        })
    }
}

#[derive(Deserialize, Clone, Debug)]
#[serde(tag = "architecture")]
pub enum Pnr {
    #[serde(rename = "ice40")]
    Ice40(NextPnrArgs<Ice40Args>),
    #[serde(rename = "ecp5")]
    Ecp5(NextPnrArgs<Ecp5Args>),
    #[serde(rename = "gowin")]
    Gowin(NextPnrArgs<GowinArgs>),
}

impl Pnr {
    pub fn pnr_tool(&self) -> Result<PnrTool> {
        Ok(match self {
            Pnr::Ice40(args) => PnrTool::NextPnr {
                pnr_binary: Utf8PathBuf::from("nextpnr-ice40"),
                args: args.clone().make_dyn("pcf")?,
                output: PnrResult::Asc(Utf8PathBuf::from("hardware.asc")),
            },
            Pnr::Ecp5(args) => PnrTool::NextPnr {
                pnr_binary: Utf8PathBuf::from("nextpnr-ecp5"),
                args: args.clone().make_dyn("lpf")?,
                output: PnrResult::Textcfg(Utf8PathBuf::from("hardware.config")),
            },
            Pnr::Gowin(args) => PnrTool::NextPnr {
                pnr_binary: Utf8PathBuf::from("nextpnr-himbaechel"),
                args: args.clone().make_dyn("cst")?,
                output: PnrResult::Json(Utf8PathBuf::from("pnrhardware.json")),
            },
        })
    }
}

#[derive(Deserialize, Clone, Debug, PartialEq)]
#[serde(tag = "tool")]
pub enum UploadTool {
    #[serde(rename = "icesprog")]
    Icesprog,
    #[serde(rename = "iceprog")]
    Iceprog,
    #[serde(rename = "tinyprog")]
    Tinyprog,
    #[serde(rename = "openocd")]
    OpenOcd { config_file: Utf8PathBuf },
    #[serde(rename = "fujprog")]
    Fujprog,
    #[serde(rename = "openFPGALoader")]
    OpenFPGALoader { board: String },
    /// Instead of running a pre-defined set of commands to upload, run the specified
    /// list of commands in a shell. #packing_result# will be replaced by the packing
    /// output
    #[serde(rename = "custom")]
    Custom { commands: Vec<String> },
}

impl UploadTool {
    pub fn binary(&self) -> &'static str {
        match self {
            UploadTool::Icesprog => "icesprog",
            UploadTool::Iceprog => "iceprog",
            UploadTool::Tinyprog => "tinyprog",
            UploadTool::OpenOcd { .. } => "openocd",
            UploadTool::Fujprog { .. } => "fujprog",
            UploadTool::OpenFPGALoader { .. } => "openFPGALoader",
            UploadTool::Custom { .. } => "sh",
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(tag = "name")]
pub enum Board {
    #[serde(rename = "ecpix-5", alias = "ecpix5")]
    Ecpix5 {
        pin_file: Option<Utf8PathBuf>,
        config_file: Option<Utf8PathBuf>,
    },
    #[serde(rename = "go-board", alias = "go", alias = "goboard")]
    GoBoard {
        pcf: Option<Utf8PathBuf>,
    },
    #[serde(rename = "tinyfpga-bx")]
    TinyfpgaBx {
        pcf: Option<Utf8PathBuf>,
    },
    Icestick {
        pcf: Option<Utf8PathBuf>,
    },
}

impl Board {
    fn upload(&self) -> Result<UploadTool> {
        match self {
            Board::Ecpix5 { config_file, .. } => config_file
                .as_ref()
                .cloned()
                .ok_or_else(|| {
                    anyhow!("field \"config_file\" required either in [board] or [upload]")
                })
                .map(|config_file| UploadTool::OpenOcd { config_file }),
            Board::GoBoard { .. } | Board::Icestick { .. } => Ok(UploadTool::Iceprog),
            Board::TinyfpgaBx { .. } => Ok(UploadTool::Tinyprog),
        }
    }

    fn pnr_ice40(
        &self,
        pcf: &Option<Utf8PathBuf>,
        package: &str,
        device: Ice40Device,
    ) -> Result<Pnr> {
        pcf.as_ref()
            .cloned()
            .ok_or_else(|| anyhow!("field \"pcf\" required either in [board] or [pnr]"))
            .map(|pcf| {
                Pnr::Ice40(NextPnrArgs {
                    device_args: Ice40Args {
                        device,
                        package: package.to_string(),
                    },
                    allow_timing_fail: false,
                    allow_unconstrained: false,
                    pin_file: pcf,
                })
            })
    }

    fn pnr(&self) -> Result<Pnr> {
        match self {
            Board::Ecpix5 { pin_file, .. } => pin_file
                .as_ref()
                .cloned()
                .ok_or_else(|| anyhow!("field \"pin_file\" required either in [board] or [pnr]"))
                .map(|pin_file| {
                    Pnr::Ecp5(NextPnrArgs {
                        device_args: Ecp5Args {
                            device: Ecp5Device::Lfe5um5g45f,
                            package: "CABGA554".to_string(),
                        },
                        allow_unconstrained: false,
                        allow_timing_fail: false,
                        pin_file,
                    })
                }),
            Board::GoBoard { pcf } => self.pnr_ice40(pcf, "vq100", Ice40Device::Ice40hx1k),
            Board::TinyfpgaBx { pcf } => self.pnr_ice40(pcf, "cm81", Ice40Device::Ice40lp8k),
            Board::Icestick { pcf } => self.pnr_ice40(pcf, "tq144", Ice40Device::Ice40lp1k),
        }
    }

    fn packing(&self) -> Result<PackingTool> {
        match self {
            Board::Ecpix5 { .. } => Ok(PackingTool::Ecppack {
                idcode: Some("0x81112043".to_string()),
            }),
            Board::GoBoard { .. } | Board::TinyfpgaBx { .. } | Board::Icestick { .. } => {
                Ok(PackingTool::Icepack)
            }
        }
    }
}

#[derive(Deserialize, Clone, Debug)]
#[serde(tag = "tool")]
pub enum PackingTool {
    #[serde(rename = "icepack")]
    Icepack,
    #[serde(rename = "ecppack")]
    Ecppack { idcode: Option<String> },
    #[serde(rename = "gowin_pack")]
    GowinPack { device: String },
}

impl PackingTool {
    pub fn binary(&self) -> &'static str {
        match self {
            PackingTool::Icepack => "icepack",
            PackingTool::Ecppack { .. } => "ecppack",
            PackingTool::GowinPack { .. } => "gowin_pack",
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "snake_case")]
#[derive(Default)]
pub enum LogOutputLevel {
    #[default]
    Full,
    Minimal,
}

pub fn default_compiler() -> Library {
    Library::Git(GitLibrary {
        url: "https://gitlab.com/spade-lang/spade.git".to_string(),
        branch: Some("main".to_string()),
        tag: None,
        commit: None,
    })
}

#[derive(Deserialize, Debug, PartialEq)]
pub struct Plugin {
    #[serde(flatten)]
    pub lib: Library,
    #[serde(default)]
    pub args: HashMap<String, String>,
}

#[derive(Debug)]
pub struct SerdeSpdxExpression(pub spdx::Expression);

impl<'de> Deserialize<'de> for SerdeSpdxExpression {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let string = String::deserialize(deserializer)?;
        let expression = spdx::Expression::parse(&string).map_err(D::Error::custom)?;
        Ok(Self(expression))
    }
}

#[derive(Deserialize, Debug, Default)]
pub struct Package {
    /// The name of the library. Must be a valid Spade identifier
    /// Anything defined in this library will be under the `name` namespace
    pub name: String,

    pub version: Option<Version>,

    pub description: Option<String>,

    pub authors: Option<Vec<String>>,

    pub license: Option<SerdeSpdxExpression>,

    #[serde(rename = "license-file")]
    pub license_file: Option<Utf8PathBuf>,

    pub readme: Option<Utf8PathBuf>,
}

/// The main project configuration specified in `swim.toml`
#[derive(Deserialize, Debug)]
pub struct Config {
    /// Library package and metadata
    #[serde(flatten)]
    pub package: Package,

    /// List of optimization passes to apply in the Spade compiler. The passes are applied
    /// in the order specified here. Additional passes specified on individual modules with
    /// #[optimize(...)] are applied before global passes.
    #[serde(default)]
    pub optimizations: Vec<String>,

    /// Where to find the Spade compiler. See [Library] for details
    #[serde(default = "default_compiler")]
    pub compiler: Library,

    /// List of commands to run before anything else.
    pub preprocessing: Option<Vec<String>>,

    /// Verilog to import in both simulation and synthesis.
    pub verilog: Option<ImportVerilog>,

    #[serde(default)]
    pub simulation: Simulation,

    pub synthesis: Option<Synthesis>,

    /// Preset board configuration which can be used instead of synthesis, pnr, packing and upload
    pub board: Option<Board>,

    pub pnr: Option<Pnr>,
    pub packing: Option<PackingTool>,
    pub upload: Option<UploadTool>,

    /// Map of libraries to include in the build.
    ///
    /// Example:
    /// ```toml
    /// [libraries]
    /// protocols = {git = https://gitlab.com/TheZoq2/spade_protocols.git}
    /// spade_v = {path = "deps/spade-v"}
    /// ```
    pub libraries: Option<HashMap<String, Library>>,

    /// Plugins to load. Specifies the location as a library, as well
    /// as arguments to the plugin
    ///
    /// Example:
    /// ```toml
    /// [plugins.loader_generator]
    /// path = "../plugins/loader_generator/"
    /// args.asm_file = "asm/blinky.asm"
    /// args.template_file = "../templates/program_loader.spade"
    /// args.target_file = "src/programs/blinky_loader.spade"
    ///
    /// [plugins.flamegraph]
    /// git = "https://gitlab.com/TheZoq2/yosys_flamegraph"
    /// ```
    ///
    /// Plugins contain a `swim_plugin.toml` which describes their behaviour.
    /// See [crate::plugin::config::PluginConfig] for details
    pub plugins: Option<HashMap<String, Plugin>>,

    #[serde(default)]
    pub log_output: LogOutputLevel,
}

macro_rules! config_getters {
    ($($fn:ident, $field:ident, $struct:ident);*$(;)?) => {
        $(
        pub fn $fn(&self) -> Result<&$struct> {
            get_field!(self, $field)
        }
        )*
    }
}

macro_rules! config_getters_with_board {
    ($($fn:ident, $field:ident, $struct:ident);*$(;)?) => {
        $(
        pub fn $fn(&self) -> Result<Cow<'_, $struct>> {
            match (&self.$field, &self.board) {
                (Some(_), _) => get_field!(self, $field).map(Cow::Borrowed),
                (None, Some(board)) => board.$field().map(Cow::Owned),
                (None, None) => bail!("either [board] or [{}] required", stringify!($field)),
            }
        }
        )*
    };
}

macro_rules! get_field {
    ($config:expr, $field:ident) => {
        $config.$field.as_ref().ok_or_else(|| {
            anyhow!(
                "swim.toml must have a [{}] section (or {} = ...) in order to run {}",
                stringify!($field),
                stringify!($field),
                stringify!($field)
            )
        })
    };
}

impl Config {
    config_getters! {
        synthesis_config, synthesis, Synthesis;
    }

    config_getters_with_board! {
        packing_config, packing, PackingTool;
        pnr_config, pnr, Pnr;
        upload_config, upload, UploadTool;
    }

    pub fn read(root: impl AsRef<Utf8Path>, forced_compiler: &Option<Utf8PathBuf>) -> Result<Self> {
        let config_str = std::fs::read_to_string(root.as_ref().join("swim.toml"))
            .context("Failed to read swim.toml")?;
        let toml_de = toml::Deserializer::new(&config_str);
        let mut unknown_fields = std::collections::HashSet::new();
        let mut config: Config = serde_ignored::deserialize(toml_de, |path| {
            unknown_fields.insert(path.to_string());
        })
        .context("Failed to parse swim.toml")?;
        if !unknown_fields.is_empty() {
            warn!(
                "swim.toml contains {} unknown field{}:",
                unknown_fields.len(),
                pluralize(unknown_fields.len())
            );
            for field in &unknown_fields {
                warn!("  {}", field);
            }
        }
        if let Some(path) = forced_compiler {
            config.compiler = Library::Path(PathLibrary { path: path.clone() })
        }
        config.replace_relative_paths(root);
        Ok(config)
    }

    /// Replace all paths in the config with absolute paths.
    pub fn replace_relative_paths(&mut self, root: impl AsRef<Utf8Path>) {
        let root = root.as_ref();

        let Config {
            package: _,
            compiler,
            preprocessing: _,
            board,
            verilog: _,
            simulation,
            synthesis: _,
            packing,
            pnr,
            upload,
            libraries,
            plugins,
            log_output: _,
            optimizations: _,
        } = self;

        match compiler {
            Library::Git(_) => (),
            Library::Path(PathLibrary { path }) => join_if_relative(root, path),
        }

        match board {
            Some(Board::Ecpix5 {
                pin_file,
                config_file,
            }) => {
                join_if_relative(root, pin_file);
                join_if_relative(root, config_file);
            }
            Some(Board::GoBoard { pcf } | Board::TinyfpgaBx { pcf } | Board::Icestick { pcf }) => {
                join_if_relative(root, pcf);
            }
            None => (),
        }

        simulation.testbench_dir_original = simulation.testbench_dir.clone();
        join_if_relative(root, &mut simulation.testbench_dir);

        match packing {
            Some(PackingTool::Icepack) => (),
            Some(PackingTool::Ecppack { idcode: _ }) => (),
            Some(PackingTool::GowinPack { .. }) => (),
            None => (),
        };

        match pnr {
            Some(Pnr::Ice40(args)) => join_if_relative(root, &mut args.pin_file),
            Some(Pnr::Ecp5(args)) => join_if_relative(root, &mut args.pin_file),
            Some(Pnr::Gowin(args)) => join_if_relative(root, &mut args.pin_file),
            None => (),
        }

        match upload {
            Some(
                UploadTool::Icesprog
                | UploadTool::Iceprog
                | UploadTool::Tinyprog
                | UploadTool::Fujprog
                | UploadTool::OpenFPGALoader { .. },
            ) => (),
            Some(UploadTool::OpenOcd { config_file }) => join_if_relative(root, config_file),
            Some(UploadTool::Custom { .. }) => (),
            None => (),
        }

        if let Some(libraries) = libraries {
            for library in libraries.values_mut() {
                match library {
                    Library::Git(_) => (),
                    Library::Path(PathLibrary { path }) => join_if_relative(root, path),
                }
            }
        }
        if let Some(plugins) = plugins {
            for plugin in plugins.values_mut() {
                match &mut plugin.lib {
                    Library::Git(_) => (),
                    Library::Path(PathLibrary { path }) => join_if_relative(root, path),
                }
            }
        }
    }

    pub fn extra_verilog_files(&self, root: &Utf8Path) -> Result<Vec<Utf8PathBuf>> {
        discarding_resolve_globs(
            root,
            self.verilog
                .as_ref()
                .and_then(|verilog| verilog.sources.as_ref())
                .unwrap_or(&vec![]),
        )
    }

    pub fn verilog_include_paths(&self, root: &Utf8Path) -> Vec<Utf8PathBuf> {
        self.verilog
            .as_ref()
            .and_then(|verilog| verilog.include.as_ref())
            .cloned()
            .unwrap_or_default()
            .into_iter()
            .map(|path| root.join(path))
            .collect()
    }

    #[cfg(test)]
    pub(crate) fn new_empty(compiler: Library) -> Self {
        Self {
            package: Package {
                name: "proj".to_string(),
                ..Default::default()
            },
            compiler,
            preprocessing: None,
            board: None,
            verilog: Default::default(),
            simulation: Default::default(),
            synthesis: None,
            packing: None,
            pnr: None,
            upload: None,
            libraries: None,
            plugins: None,
            log_output: LogOutputLevel::Full,
            optimizations: vec![],
        }
    }
}

/// If the given path is a relative path, join it with a prefix to make it absolute.
fn join_if_relative<'p>(root: &Utf8Path, path: impl Into<Option<&'p mut Utf8PathBuf>>) {
    assert!(root.is_absolute());

    if let Some(path) = path.into() {
        if path.is_relative() {
            let _ = std::mem::replace(path, root.join(&path));
        }
    }
}

/// Expands `glob` into a vec of matches. If there is a pattern error in the glob, it is returned.
/// If not, a list of matches or errors are returned.
///
/// The latter can occur if the glob expands into a directory with no read permission, for example.
/// In this case, it is probably best to warn while a pattern error should be a hard error.
fn resolve_glob(root: &Utf8Path, glob: &str) -> Result<Vec<Result<Utf8PathBuf>>> {
    // If this is an absolute path, don't add the root
    let absolute_glob = if glob.starts_with("/") {
        glob.to_string()
    } else {
        format!("{root}/{glob}")
    };

    Ok(glob::glob(&absolute_glob)
        .with_context(|| "Error in {glob}")?
        .map(|entry| {
            entry
                .with_context(|| "When expanding {glob}")
                .and_then(|path| {
                    Utf8PathBuf::try_from(path).with_context(|| "Glob exapnded to non-utf-8 path")
                })
        })
        .collect())
}

/// Runs [resolve_glob] on each glob, returning an error if the pattern expansion failed for any of
/// them, but downgrading GlobErrors to warnings
fn discarding_resolve_globs(root: &Utf8Path, globs: &[String]) -> Result<Vec<Utf8PathBuf>> {
    Ok(globs
        .iter()
        .map(|g| resolve_glob(root, g))
        .collect::<Result<Vec<_>>>()?
        .into_iter()
        .flatten()
        .filter_map(|p| {
            p.map_err(|e| {
                warn!("Glob expansion error\n{e:#?}");
                e
            })
            .ok()
        })
        .collect())
}

#[cfg(test)]
mod tests {
    use camino::Utf8PathBuf;
    use std::collections::HashMap;

    use crate::{config::Plugin, libraries::GitRef};

    use super::{
        discarding_resolve_globs, Config, Library, PathLibrary, Simulation, Synthesis, UploadTool,
    };

    #[test]
    fn replaces_relative_dirs() {
        // NOTE: This config is deliberately made up.
        let mut config: Config = toml::from_str(&unindent::unindent(
            r#"
            name = "test"
            description = "A testing swim.toml"
            authors = ["Foo", "Bar"]
            version = "0.3.4"
            license = "MIT OR Apache-2.0"
            license-file = "../LICENSE.txt"
            compiler = { path = "/my/own/spade" }
            preprocessing = ["python3"]
            verilog.sources = ["aaaa.v"]

            [simulation]
            testbench_dir = "tests"

            [synthesis]
            top = "bottom"
            command = "owo"
            verilog.sources = ["bbbb.v"]

            [upload]
            tool = "openocd"
            config_file = "ocd.conf"

            [libraries]
            lib = { path = "dem" }

            [plugins]
            plugin = { path = "dem" }
            "#,
        ))
        .unwrap();

        config.replace_relative_paths("/root/");

        let Config {
            package,
            compiler,
            preprocessing,
            board: _,
            verilog,
            simulation,
            synthesis,
            packing: _,
            pnr: _,
            upload,
            libraries,
            plugins,
            log_output: _,
            optimizations: _,
        } = config;

        assert_eq!(package.name, "test");

        assert_eq!(
            compiler,
            Library::Path(PathLibrary {
                path: Utf8PathBuf::from("/my/own/spade"),
            })
        );

        assert_eq!(
            verilog
                .as_ref()
                .and_then(|verilog| verilog.sources.as_ref()),
            Some(&vec![String::from("aaaa.v")])
        );
        assert_eq!(preprocessing, Some(vec![String::from("python3")]));
        assert_eq!(
            simulation,
            Simulation {
                testbench_dir: Utf8PathBuf::from("/root/tests"),
                testbench_dir_original: Utf8PathBuf::from("tests"),
                python_deps: None,
                simulator: "icarus".to_owned(),
                cpp_version: None,
                verilator_args: None,
            },
        );
        assert_eq!(
            synthesis,
            Some(Synthesis {
                top: "bottom".to_string(),
                command: "owo".to_string(),
                verilog: Some(crate::config::ImportVerilog {
                    include: None,
                    sources: Some(vec![String::from("bbbb.v")])
                }),
            }),
        );
        assert_eq!(
            upload,
            Some(UploadTool::OpenOcd {
                config_file: Utf8PathBuf::from("/root/ocd.conf")
            })
        );
        assert_eq!(
            libraries,
            Some(HashMap::from([(
                "lib".to_string(),
                Library::Path(PathLibrary {
                    path: Utf8PathBuf::from("/root/dem/"),
                })
            )])),
        );
        assert_eq!(
            plugins,
            Some(HashMap::from([(
                "plugin".to_string(),
                Plugin {
                    lib: Library::Path(PathLibrary {
                        path: Utf8PathBuf::from("/root/dem/"),
                    }),
                    args: HashMap::new()
                }
            )])),
        );
    }

    #[test]
    fn discarding_resolve_globs_finds_direct_matches() {
        let root = Utf8PathBuf::try_from(project_root::get_project_root().unwrap()).unwrap();
        let files = discarding_resolve_globs(&root, &["src/main.rs".to_string()]);

        assert_eq!(files.unwrap(), vec![root.join("src/main.rs")]);
    }

    #[test]
    fn discarding_resolve_globs_finds_pattern_matches() {
        let root = Utf8PathBuf::try_from(project_root::get_project_root().unwrap()).unwrap();
        let files = discarding_resolve_globs(&root, &["src/main*".to_string()]);

        assert_eq!(files.unwrap(), vec![root.join("src/main.rs")]);
    }

    #[test]
    fn discarding_resolve_globs_absolute_finds_pattern_matches() {
        let root = Utf8PathBuf::try_from(project_root::get_project_root().unwrap()).unwrap();
        let files = discarding_resolve_globs(&root, &[root.join("src/main*").to_string()]);

        assert_eq!(files.unwrap(), vec![root.join("src/main.rs")]);
    }

    #[test]
    fn overriding_default_refspec_works() {
        let config: Config = toml::from_str(&unindent::unindent(
            r#"
            name = "test"

            [libraries]
            lib1 = { git = "lib1" }
            lib2 = { git = "lib1", branch = "other_branch" }
            lib3 = { git = "lib1", tag = "other_tag" }

            [plugins]
            plugin = { path = "dem" }
            "#,
        ))
        .unwrap();

        match config.libraries.as_ref().unwrap().get("lib1").unwrap() {
            Library::Git(git) => assert_eq!(
                GitRef::parse_git_library(git.clone()).unwrap(),
                GitRef::Branch("main".to_string())
            ),
            Library::Path(_) => panic!("Got path dep"),
        }

        match config.libraries.as_ref().unwrap().get("lib2").unwrap() {
            Library::Git(git) => assert_eq!(
                GitRef::parse_git_library(git.clone()).unwrap(),
                GitRef::Branch("other_branch".to_string())
            ),
            Library::Path(_) => panic!("Got path dep"),
        }

        match config.libraries.as_ref().unwrap().get("lib3").unwrap() {
            Library::Git(git) => assert_eq!(
                GitRef::parse_git_library(git.clone()).unwrap(),
                GitRef::Tag("other_tag".to_string())
            ),
            Library::Path(_) => panic!("Got path dep"),
        }
    }
}
