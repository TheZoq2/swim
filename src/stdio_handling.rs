use color_eyre::eyre::{bail, Result};
use lazy_static::lazy_static;
use log::{info, warn};
use std::{
    future::pending,
    io::{stdout, IsTerminal},
    process::{ExitStatus, Stdio},
    sync::Arc,
    time::Duration,
};

use async_trait::async_trait;
use tokio::{
    io::AsyncReadExt,
    process::{ChildStderr, ChildStdout, Command},
    sync::Mutex,
    time::{self, Instant},
};

lazy_static! {
    static ref SHORT_LIVED_DURATION: Duration = Duration::from_millis(100);
}

pub fn select_short_lived_io_handler(
    message: String,
    quiet: bool,
    timeout_override: Option<Duration>,
) -> Box<(dyn StdioHandler + Send)> {
    if quiet {
        Box::new(StdioAccumulator::new(Arc::new(Mutex::new(String::new()))))
    } else {
        let io_inheriter = ShortLivedStdioHandler::new(message);

        let io_inheriter = if let Some(t) = timeout_override {
            io_inheriter.with_timeout(t)
        } else {
            io_inheriter
        };
        Box::new(io_inheriter)
    }
}

#[async_trait]
pub trait CommandRunningExt {
    async fn wait_with_stdout_handler<'a>(
        &'a mut self,
        handler: &'a mut (dyn StdioHandler + Send),
    ) -> Result<ExitStatus>;

    /// Runs the command to completion and error in case spawning fails or the status
    /// is not Success. If the command takes shorter than SHORT_LIVED_DURATION to complete,
    /// its output will not be printed, only a spinner and message will be shown while executing.
    /// If the command takes longer than SHORT_LIVED_DURATION, the full stdout and stderr of
    /// the command will be output as if the command was run with `.status()`.
    ///
    /// If swim is being run in non-interactive mode, this function simply calls `.status()?` and
    /// bails if the exit status is not 0.
    async fn run_short_lived_expect_success(&mut self, message: String) -> color_eyre::Result<()>;
}

#[async_trait]
impl CommandRunningExt for Command {
    async fn wait_with_stdout_handler<'a>(
        &'a mut self,
        handler: &'a mut (dyn StdioHandler + Send),
    ) -> Result<ExitStatus> {
        self.stdout(handler.cmd_out());
        self.stderr(handler.cmd_err());

        let mut cmd = self.spawn()?;

        let mut stdout = cmd.stdout.take();
        let mut stderr = cmd.stderr.take();

        let mut status = Box::pin(cmd.wait());
        let mut tick_timer = time::interval(time::Duration::from_millis(100));

        loop {
            tokio::select! {
                result = &mut status => {
                    handler.cleanup().await;
                    break Ok(result?)
                }
                _ = handler.process(&mut stdout, &mut stderr) => {}
                _ = tick_timer.tick() => {
                    handler.tick().await;
                },
            }
        }
    }

    async fn run_short_lived_expect_success(&mut self, message: String) -> color_eyre::Result<()> {
        let mut handler = ShortLivedStdioHandler::new(message);
        let status = self.wait_with_stdout_handler(&mut handler).await?;

        if !status.success() {
            println!("{}", handler.stdio);
            bail!("Command exited with non-zero status")
        } else {
            Ok(())
        }
    }
}

#[async_trait]
pub trait StdioHandler {
    fn cmd_out(&self) -> Stdio;
    fn cmd_err(&self) -> Stdio;

    // Start processing the stdio
    async fn process(
        &mut self,
        stdout: &mut Option<ChildStdout>,
        stderr: &mut Option<ChildStderr>,
    ) -> Result<()>;

    async fn extra_stdout(&mut self, extra: &str);

    /// Optional function that gets called at a 100Hz rate
    async fn tick(&mut self) {}
    /// Optional function for cleaning things up once a command is done
    async fn cleanup(&mut self) {}

    /// Optional function that allows reading the accumulated stdio if the handler didn't
    /// print it already
    async fn get_remaining_stdio(&self) -> Option<String>;
}

pub struct StdioInherit;

#[async_trait]
impl StdioHandler for StdioInherit {
    fn cmd_out(&self) -> Stdio {
        Stdio::inherit()
    }

    fn cmd_err(&self) -> Stdio {
        Stdio::inherit()
    }

    async fn process(
        &mut self,
        _stdout: &mut Option<ChildStdout>,
        _stderr: &mut Option<ChildStderr>,
    ) -> Result<()> {
        pending().await
    }

    async fn extra_stdout(&mut self, extra: &str) {
        println!("{}", extra)
    }

    async fn get_remaining_stdio(&self) -> Option<String> {
        None
    }
}

pub struct StdioAccumulator {
    stdio: Arc<Mutex<String>>,
}

impl StdioAccumulator {
    pub fn new(stdio: Arc<Mutex<String>>) -> Self {
        Self { stdio }
    }
}

#[async_trait]
impl StdioHandler for StdioAccumulator {
    fn cmd_out(&self) -> Stdio {
        Stdio::piped()
    }

    fn cmd_err(&self) -> Stdio {
        Stdio::piped()
    }

    async fn process(
        &mut self,
        stdout: &mut Option<ChildStdout>,
        stderr: &mut Option<ChildStderr>,
    ) -> Result<()> {
        let stdout = stdout.as_mut().unwrap();
        let stderr = stderr.as_mut().unwrap();
        tokio::select! {
            new = read_to_vec(stdout) => {
                *self.stdio.lock().await += &String::from_utf8_lossy(&new?);
            }
            new = read_to_vec(stderr) => {
                *self.stdio.lock().await += &String::from_utf8_lossy(&new?);
            }
        };
        Ok(())
    }

    async fn extra_stdout(&mut self, extra: &str) {
        *self.stdio.lock().await += extra;
    }

    async fn get_remaining_stdio(&self) -> Option<String> {
        Some(self.stdio.lock().await.clone())
    }
}

// Reads stdout into the contained mutex, prints stderr
pub struct StdoutCollector {
    stdout: Arc<Mutex<String>>,
}

impl StdoutCollector {
    pub fn new(stdout: Arc<Mutex<String>>) -> Self {
        Self { stdout }
    }
}

#[async_trait]
impl StdioHandler for StdoutCollector {
    fn cmd_out(&self) -> Stdio {
        Stdio::piped()
    }

    fn cmd_err(&self) -> Stdio {
        Stdio::inherit()
    }

    async fn process(
        &mut self,
        stdout: &mut Option<ChildStdout>,
        _stderr: &mut Option<ChildStderr>,
    ) -> Result<()> {
        let stdout = stdout.as_mut().unwrap();
        tokio::select! {
            new = read_to_vec(stdout) => {
                *self.stdout.lock().await += &String::from_utf8_lossy(&new?);
            }
        };
        Ok(())
    }

    async fn extra_stdout(&mut self, extra: &str) {
        *self.stdout.lock().await += extra
    }

    async fn get_remaining_stdio(&self) -> Option<String> {
        warn!("(Internal) Trying to get remaining stdio from StdoutCollector");
        None
    }
}

async fn read_to_vec(stream: &mut (impl AsyncReadExt + Unpin)) -> Result<Vec<u8>> {
    let mut buf = vec![];
    stream.read_buf(&mut buf).await?;
    Ok(buf)
}

pub struct StdioIgnorer;

#[async_trait]
impl StdioHandler for StdioIgnorer {
    fn cmd_out(&self) -> Stdio {
        Stdio::piped()
    }

    fn cmd_err(&self) -> Stdio {
        Stdio::piped()
    }

    async fn process(
        &mut self,
        _stdout: &mut Option<ChildStdout>,
        _stderr: &mut Option<ChildStderr>,
    ) -> Result<()> {
        pending().await
    }

    async fn extra_stdout(&mut self, _: &str) {}

    async fn get_remaining_stdio(&self) -> Option<String> {
        warn!("(Internal) Trying to get remaining stdio from StdioIgnorer");
        None
    }
}

pub struct ShortLivedStdioHandler {
    message: String,
    /// This field contains the stdio of the command unless it has been printed already
    pub stdio: String,
    duration: Duration,
    expired: bool,
    progress_tracking: Option<(Instant, indicatif::ProgressBar)>,
}

impl ShortLivedStdioHandler {
    pub fn new(message: String) -> Self {
        Self {
            message,
            stdio: String::new(),
            duration: *SHORT_LIVED_DURATION,
            expired: false,
            progress_tracking: None,
        }
    }

    pub fn with_timeout(mut self, duration: Duration) -> Self {
        self.duration = duration;
        self
    }
}

#[async_trait]
impl StdioHandler for ShortLivedStdioHandler {
    fn cmd_out(&self) -> Stdio {
        Stdio::piped()
    }

    fn cmd_err(&self) -> Stdio {
        Stdio::piped()
    }

    async fn process(
        &mut self,
        stdout: &mut Option<ChildStdout>,
        stderr: &mut Option<ChildStderr>,
    ) -> Result<()> {
        let stdout = stdout.as_mut().unwrap();
        let stderr = stderr.as_mut().unwrap();

        if !std::io::stdout().is_terminal() {
            if !self.expired {
                info!("{}", self.message);
                self.expired = true;
            }
            let handle = |new| {
                let new = String::from_utf8_lossy(new);
                print!("{new}")
            };

            tokio::select! {
                new = read_to_vec(stdout) => {
                    handle(&new?);
                }
                new = read_to_vec(stderr) => {
                    handle(&new?);
                }
            };
        } else {
            if self.progress_tracking.is_none() {
                self.progress_tracking = Some((
                    Instant::now(),
                    indicatif::ProgressBar::new_spinner().with_message(self.message.clone()),
                ));
            }
            let (start_time, spinner) = self.progress_tracking.as_mut().unwrap();

            if (Instant::now() - *start_time) > self.duration && !self.expired {
                self.expired = true;
                spinner.suspend(|| {
                    info!("{}", self.message);
                    print!("{}", self.stdio)
                })
            }

            let mut handle = |new| {
                let new = String::from_utf8_lossy(new);
                if self.expired {
                    spinner.suspend(|| print!("{new}"))
                } else {
                    self.stdio += &new;
                }
            };

            tokio::select! {
                new = read_to_vec(stdout) => {
                    handle(&new?)
                }
                new = read_to_vec(stderr) => {
                    handle(&new?)
                }
            };
        }

        Ok(())
    }

    async fn tick(&mut self) {
        if let Some((_, spinner)) = self.progress_tracking.as_mut() {
            if stdout().is_terminal() {
                spinner.tick();
            }
        }
    }

    async fn extra_stdout(&mut self, extra: &str) {
        self.stdio += extra
    }

    async fn cleanup(&mut self) {
        if let Some((_, spinner)) = self.progress_tracking.as_mut() {
            spinner.finish_and_clear();
            // If the command was long running, we'll print an info to highlight the start of the
            // next command
            if self.expired {
                info!("Finished: {}", self.message);
            }
        }
    }

    async fn get_remaining_stdio(&self) -> Option<String> {
        Some(self.stdio.clone())
    }
}
