## Author checklist

- [ ] Added a changelog entry, if relevant
  - You can use `./add_changelog <MR number or a_descriptive_name>` to do this
