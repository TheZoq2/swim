use std::process::Command;

fn main() {
    let hash = source_version()
        .map(|hash| format!("-{}", hash))
        .unwrap_or_default();
    let version = format!("v{}{}", env!("CARGO_PKG_VERSION"), hash);

    println!("cargo:rustc-env=VERSION={}", version);
}

fn source_version() -> Option<String> {
    let rev_count = Command::new("git")
        .arg("rev-list")
        .arg("--count")
        .arg("HEAD")
        .current_dir(env!("CARGO_MANIFEST_DIR"))
        .output()
        .ok()?;
    let rev_parse = Command::new("git")
        .arg("rev-parse")
        .arg("--short")
        .arg("HEAD")
        .current_dir(env!("CARGO_MANIFEST_DIR"))
        .output()
        .ok()?;
    Some(format!(
        "r{}-{}",
        String::from_utf8_lossy(&rev_count.stdout).trim(),
        String::from_utf8_lossy(&rev_parse.stdout).trim(),
    ))
}
