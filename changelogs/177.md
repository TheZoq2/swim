<!--
 Thanks for the MR! Please add lines describing your changes in the appropriate section

 For example:

## Added
- Added some more fish
## Fixed
 a generic parameter
-->

## Added

- Config file now has `description`, `version`, `authors`, `license`, and `license-file` fields

## Fixed

## Changed

## Removed


