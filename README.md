# Swim

<!-- image of pheobe swimming through code libraries or something needed -->

Swim is a batteries-included build system and package manager for the Spade programming language. 
It manages rebuilds of Spade source code, the compiler and any additional Verilog, supports simulation using your favorite simulators (icarus, verilator), and automates synthesis for ECP5 and iCE40 using [yosys](https://github.com/YosysHQ/yosys) and [nextpnr](https://github.com/YosysHQ/nextpnr). 
The generated Verilog can also, of course, be used with any other tool.

See the [documentation](https://docs.spade-lang.org/swim/index.html) for details
on installation and usage.

## License

Swim is licensed under the [EUPL-1.2 license](LICENSE.txt).
